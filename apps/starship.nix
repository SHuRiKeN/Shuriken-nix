{
  programs.starship = {
    enable = true;
    enableFishIntegration = true;
    settings = {
      username = {
        style_user = "green bold";
	style_root = "red bold";
        format = "[$user]($style) ";
        disabled = false;
        show_always = true;
      };
      hostname = {
        ssh_only = false;
        format = "on [$hostname](bold purple) ";
        trim_at = ".";
        disabled = false;
      };
      character = {
        success_symbol = "[➜](bold green)";
	error_symbol = "[✗](bold red)";
      };
    };
  };
}

