{ pkgs, lib, config, ... }:

{ 
  programs.firefox = let 
    betterfox = pkgs.fetchFromGitHub {
      owner = "yokoffing";
      repo = "Betterfox";
      rev = "68c1d0cefcda61812b023772198013c57363fd94";
      hash = "sha256-xwTSYnXGxAMYZR7T1ROWChhdbd3dV/ndTzKo9PC7eLo=";
    };
    finalUserJS = lib.strings.concatStrings [
      (builtins.readFile "${betterfox}/user.js")
    ];
  in {
    enable = true;
    profiles.default = {
      extensions = with pkgs.nur.repos.rycee.firefox-addons; [
        ublock-origin
	tabliss
	canvasblocker
	darkreader
	metamask
      ];
      extraConfig = finalUserJS;
      search = {
        engines = {
          "Disroot" = {
            urls = [{template = "https://search.disroot.org/search?q={searchTerms}"; }];
	    iconUpdateURL = "https://search.disroot.org/static/themes/beetroot/img/searxng.png";
	    updateInterval = 24 * 60 * 60 * 1000;
	    definedAliases = [ "@ds" ];
          };
        };
	default = "Disroot";
	force = true;
      };
      settings = {
        "svg.context-properties.content.enabled" = true;
      };
      userChrome = ''
        @import "firefox-gnome-theme/userChrome.css";
	@import "firefox-gnome-theme/theme/colors/dark.css";
      '';
      userContent = ''
        @import "firefox-gnome-theme/userContent.css;"
      '';
    };
  };
}
