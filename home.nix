{ config, lib, pkgs, ... }:

{
  imports = [
    ./apps/starship.nix
    ./apps/firefox.nix
  ];
  home.username = "shuriken";
  home.homeDirectory = "/home/shuriken";
  home.stateVersion = "23.05";
  home.packages = with pkgs; [ 
    blackbox-terminal
    wget
    fd
    ripgrep
  ];

  programs.neovim = {
    enable = true;
    defaultEditor = true;
    vimAlias = true;
    vimdiffAlias = true;
  };

  programs.fish = {
    enable = true;
  };

  programs.git = {
    enable = true;
    userEmail = "shuriken@disroot.org";
    userName = "SHuRiKeN";
    extraConfig = {
      init = {
        defaultBranch = "main";
      }; 
    };
  };

  gtk = {
    enable = true;
    theme = {
      name = "adw-gtk3-dark";
      package = pkgs.adw-gtk3;
      };
    iconTheme = {
      name = "Papirus-Dark";
      package = pkgs.papirus-icon-theme;
    };
  };

  dconf.settings = {
    "org/gnome/desktop/interface" = {
      color-scheme = "prefer-dark";
    };
    "org/gnome/desktop/peripherals/touchpad" = {
      click-method = "areas";
    };
  };
}

